package co.com.project.logs.configuration;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.logs.domain.util.DeserializerLogger;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddressed;

    @Bean
    public ConsumerFactory<String, LoggerDTO> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddressed);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "consumers");
        return new DefaultKafkaConsumerFactory<>(
            props,
            new StringDeserializer(),
            new DeserializerLogger());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, LoggerDTO> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, LoggerDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
