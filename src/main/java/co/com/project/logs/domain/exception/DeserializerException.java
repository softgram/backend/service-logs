package co.com.project.logs.domain.exception;

import co.com.project.common.domain.exception.ApplicationException;
import lombok.experimental.StandardException;

@StandardException
public class DeserializerException extends ApplicationException {
}
