package co.com.project.logs.domain.util;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.common.domain.model.ExceptionBody;
import co.com.project.logs.domain.exception.DeserializerException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;

public class DeserializerLogger implements Deserializer<LoggerDTO> {
    @Override
    public LoggerDTO deserialize(String topic, byte[] data) {
        ObjectMapper mapper = new ObjectMapper();
        try {JsonNode jsonNode = mapper.readTree(data);
            LoggerDTO logger = mapper.readValue(data, LoggerDTO.class);

            Object object = logger.getData();

            if(logger.getStatus() >= 400) {
                object = mapper.readValue(jsonNode.get("data").toString(), ExceptionBody.class);
            }

            return LoggerDTO
                        .init()
                            .data(object)
                            .service(logger.getService())
                            .status(logger.getStatus())
                            .message(logger.getMessage())
                        .build();
        } catch (IOException e) {
            throw new DeserializerException();
        }
    }
}
