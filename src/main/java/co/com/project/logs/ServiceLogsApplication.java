package co.com.project.logs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ServiceLogsApplication {

	public static void main(String... args) {
		SpringApplication.run(ServiceLogsApplication.class);
	}

}
