package co.com.project.logs.adapter.inbound.kafka.consumer;

import co.com.project.common.adapter.inbound.web.dto.LoggerDTO;
import co.com.project.common.adapter.util.JsonUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LoggerKafkaConsumer {
    @KafkaListener(topics = "${spring.kafka.topics.logs}")
    public void loggedMessage(LoggerDTO logger) {
        log.info(logger.getService().toUpperCase());
        if(logger.getStatus() >= 400) {
            log.error("Exception: \n".concat(JsonUtility.toJson(logger.getData())));
            log.error("Status: ".concat(logger.getStatus().toString()));
            log.error(logger.getMessage());
        } else {
            log.info("Data returning: ".concat(logger.getData().toString()));
            log.info("Status: ".concat(logger.getStatus().toString()));
            log.info(logger.getMessage());
        }
    }
}
